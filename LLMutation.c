//John Holland
//Mutation Assignment
//Linked List Implementation


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define NUM_CANDIDATES 1000 //must be evenly divisibly by 4
#define TARGET_LEN 44
#define TARGET "the quick brown fox jumped over the lazy dog"
#define MUTATION_RATE 1
#define TRUE 1
#define FALSE 0


typedef struct CANDIDATENODE//Nodes used in building Linked Lists
{
	char sentence[TARGET_LEN+1];
	int rank;
	int score;
	struct CANDIDATENODE *next;

} Candidate;


void print();
void scoreSentence(Candidate *can);
void randomSentence(Candidate *can);
char randomChar();
void breedSentences(Candidate *can1);
void Sort();
int checkSentences();


char* alpha = "abcdefghijklmnopqrstuvwxyz ";//alphabet
Candidate *root; //the root of the list      
Candidate *pointerCurrent; // the second pointer used for going through list
Candidate *clearPointer; 
int generation = 0; //counts num of times that breeds and mutates happen


int main(void)
{
	int counter = 0;
	FILE *file = fopen("LLoutput.txt", "w");

	if (file == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}

	srand(time(NULL));//randomizes seed

	root = malloc(sizeof(Candidate));
	pointerCurrent = malloc(sizeof(Candidate));


	pointerCurrent = root;//set pointer to the head

	while (counter < NUM_CANDIDATES-1) 
	{
		pointerCurrent->next = malloc(sizeof(Candidate)); 
		pointerCurrent = pointerCurrent->next; 
		counter++;
	}



	pointerCurrent = root;//point at root again
	counter =1;//reuse counter
	while (pointerCurrent != NULL)
	{

		randomSentence(pointerCurrent);//fill with random string
		scoreSentence(pointerCurrent);// score the sentences
		pointerCurrent->rank = counter;//rank the sentences
		pointerCurrent = pointerCurrent->next;
		counter++;
	}



	//sort after initialization
	Sort();

	while (checkSentences() != TRUE)// until we find a correct sentence
	{
		int counter = 0;
		int goodSentences = NUM_CANDIDATES/2; //create way to force even 
		int mutationNums = MUTATION_RATE * NUM_CANDIDATES;


		//breed  
		pointerCurrent = root;


		clearPointer = root;


		while (counter < goodSentences)
		{
			clearPointer = clearPointer->next;
			counter++;
		}


		while (clearPointer->next != NULL) 
		{
			breedSentences(pointerCurrent);//advancing happens inside breedSentences

		}


		//mutate
		counter = 0;


		while (counter < mutationNums)
		{
			int r = rand();
			int sentenceIndex=0;
			int randomSentenceIndex = r % NUM_CANDIDATES;
			int randomLetterIndex = r % TARGET_LEN;
			char randC = randomChar();

			pointerCurrent = root;

			while (sentenceIndex < randomSentenceIndex)
			{
				pointerCurrent= pointerCurrent->next;
				sentenceIndex++;
			}

			pointerCurrent->sentence[randomLetterIndex]= randC;
			counter++;
		}


		for (pointerCurrent = root; pointerCurrent!= NULL; pointerCurrent = pointerCurrent->next)
		{
			scoreSentence(pointerCurrent);
		}


		//sort
		Sort();
		print(file, root);

		generation++;
	}






}


void Sort()//Insertion Sort using two Linked List
{
	int counter;
	Candidate *sortedListRoot=NULL;
	Candidate *sortedPointer=NULL; 

	sortedListRoot = root;	
	root = root->next; 

	sortedListRoot->next = NULL;
	sortedPointer =sortedListRoot;

	while (root!= NULL) 
	{
		Candidate *tempUnsorted;

		if (root->score > sortedListRoot->score) 
		{
			Candidate *temp = root->next;

			root->next = sortedListRoot;
			sortedListRoot = root;

			root = temp;

		}
		else
		{
			sortedPointer = sortedListRoot;

			while (sortedPointer->next!=NULL && sortedPointer->next->score > root->score)
			{

				sortedPointer = sortedPointer->next;

			}

			tempUnsorted = root->next;
			root->next = sortedPointer->next;
			sortedPointer->next = root;
			root = tempUnsorted;
		}

	}

	//after sorting, 

	root = sortedListRoot;
	sortedPointer = root;

	for (counter =1; sortedPointer != NULL; sortedPointer=sortedPointer->next)
	{
		sortedPointer->rank = counter;
		counter++;
	}

}



void breedSentences(Candidate *can1)
{

	Candidate *can2 = can1->next;

	char childOne[TARGET_LEN+1];
	char childTwo[TARGET_LEN+1];

	int pivot1  = rand() %TARGET_LEN-1;
	int pivot2  = rand() %TARGET_LEN-1;

	int i;
	for (i =0; i<TARGET_LEN; i++) 
	{
		if (i<pivot1)
		{
			childOne[i]= can1->sentence[i];
		}
		else
		{
			childOne[i]= can2->sentence[i];
		}


		if (i<pivot2)
		{
			childTwo[i]= can1->sentence[i];
		}
		else
		{
			childTwo[i]= can2->sentence[i];
		}

		childOne[TARGET_LEN]= '\0';
		childTwo[TARGET_LEN]= '\0';
	}

	strcpy(clearPointer->sentence, childOne);
	clearPointer = clearPointer->next;
	strcpy(clearPointer->sentence, childTwo);	

	if (clearPointer->next == NULL) {
		return;
	}
	clearPointer = clearPointer->next; 
	pointerCurrent = pointerCurrent->next->next;

}

int checkSentences()//checks through whole list to see if any nodes hold a correct sentence
{
	Candidate *pointerCurrent = root;

	while (pointerCurrent != NULL ) 
	{
		if (strcmp(pointerCurrent->sentence, TARGET) == 0)
		{
			return 1;
		}
		pointerCurrent = pointerCurrent->next;
	}
	return 0;
}

void scoreSentence(Candidate *can)//gives the candidate sentence a score = to the num of correct characters in the right place
{

	int scores=0;
	char correct[TARGET_LEN+1]; 
	int counter =0;

	strcpy(correct, TARGET);

	while (counter < TARGET_LEN) {

		if(can->sentence[counter] == correct[counter])
		{
			scores++;

		}
		counter++;
	}

	can->score = scores;
}


char randomChar()//returns a random lowercase letter or a space
{
	char newCharacter;
	int r = rand();
	int randomCharacterIndex = r % strlen(alpha);

	newCharacter = alpha[randomCharacterIndex];
	return newCharacter;
}


void randomSentence(Candidate *can)//fills node with random sentence
{
	int counter =0;

	while(counter<TARGET_LEN)
	{
		can->sentence[counter] = randomChar();
		counter++;
	}
	can->sentence[counter] = '\0';
}

void print(FILE *file, Candidate *head)// prints best sentence in generation, uncomment for testing
{
	Candidate *pointer = head;
	fprintf(file, "Gen: %d : %s : %d\n", generation, pointer->sentence, pointer->score);
	printf("Gen: %d : %s : %d\n", generation, pointer->sentence, pointer->score);
}

