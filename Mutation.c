//John Holland
//Mutation Assignment

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define NUM_CANDIDATES 5000
#define TARGET_LEN 20
#define TARGET "the quick brown fox"
#define MUTATION_RATE 1

#define TRUE 1
#define FALSE 0


typedef struct CANDIDATE 
{
	char sentence[TARGET_LEN+1];
	int score;
	
} Candidate;



void rankSentences();
void orderSentences();
void scoreSentences();
void muteSentences();
void breedSentences();
void doSuccessPrints();
int checkSentences();
void getLine();
void randomSentence();
char randomChar();

char* alpha = "abcdefghijklmnopqrstuvwxyz ";
Candidate topcandidateSentences[NUM_CANDIDATES];
int main(void)
{


		
	int sentencesFilledCounter = 0;// index and counter for while loop
	int sortIndex;
	int gen =0;
	
	FILE *file = fopen("output.txt", "w");

    if (file == NULL)
    {
		printf("Error opening file!\n");
		exit(1);
    }
	
	srand(time(NULL)); // set seed to random number
	
	while (sentencesFilledCounter< NUM_CANDIDATES) //fill sentences with randomness for the first time
	{
		randomSentence(&topcandidateSentences[sentencesFilledCounter]);// passes in location of indexed sentence
		scoreSentences(&topcandidateSentences[sentencesFilledCounter]);//after generating sentence, find its score
		
		
		//code below should rank sentences by score 
		
		for (sortIndex = sentencesFilledCounter; sortIndex >= 1; sortIndex--) 
		{
			if (topcandidateSentences[sortIndex].score > topcandidateSentences[sortIndex-1].score)
			{
				
				char* tempSent = (char*) malloc(sizeof(topcandidateSentences[sortIndex].sentence));
				int tempScore = 0;
				
				strcpy(tempSent, topcandidateSentences[sortIndex].sentence);
				strcpy(topcandidateSentences[sortIndex].sentence, topcandidateSentences[sortIndex-1].sentence);
				strcpy(topcandidateSentences[sortIndex-1].sentence, tempSent);
				
				
				tempScore = topcandidateSentences[sortIndex].score;
				topcandidateSentences[sortIndex].score = topcandidateSentences[sortIndex-1].score;
				topcandidateSentences[sortIndex-1].score = tempScore;
				
			}
		}
		sentencesFilledCounter++;
	}
	
	while (checkSentences() != TRUE) 
	{	
		
		//breed
		int index = 0;
		int countIndex = 0;
		int counter = 0;
		int reCounter = 0;
		int mutationNums = MUTATION_RATE * NUM_CANDIDATES;


		while (index < (NUM_CANDIDATES/2))
		{
			breedSentences(&topcandidateSentences[index], &topcandidateSentences[index+1], NUM_CANDIDATES-index);
			index+=2;
		}
		
		//mutate
		while (counter < mutationNums)
		{
			int r = rand();	
			int randomSentenceIndex = r % NUM_CANDIDATES;
			int randomLetterIndex = r % TARGET_LEN-1;
			
			topcandidateSentences[randomSentenceIndex].sentence[randomLetterIndex]= randomChar();
			counter++;
		}
		
		
		
		//rescore and rerank
		while (reCounter < NUM_CANDIDATES)
		{
			scoreSentences(&topcandidateSentences[reCounter]);
			reCounter++;
		}
		
		

		
		for (countIndex = sentencesFilledCounter; countIndex >= 1; countIndex--) 
		{
			if (topcandidateSentences[countIndex].score > topcandidateSentences[countIndex-1].score)
			{
				
				char* tempSent = (char*) malloc(sizeof(topcandidateSentences[countIndex].sentence));
				int tempScore = 0;
				
				strcpy(tempSent, topcandidateSentences[countIndex].sentence);
				strcpy(topcandidateSentences[countIndex].sentence, topcandidateSentences[countIndex-1].sentence);
				strcpy(topcandidateSentences[countIndex-1].sentence, tempSent);
				
				
				tempScore = topcandidateSentences[countIndex].score;
				topcandidateSentences[countIndex].score = topcandidateSentences[countIndex-1].score;
				topcandidateSentences[countIndex-1].score = tempScore;
				
			}
		}
		
		
		fprintf(file, "GEN= %d; Sentence= '%s'; Score= %d\n", gen, topcandidateSentences[0].sentence,topcandidateSentences[0].score );
		printf("GEN= %d; Sentence= '%s'; Score= %d\n", gen, topcandidateSentences[0].sentence,topcandidateSentences[0].score );
		gen++;
		
		/*
		//See the relative rank of the two children
		int newCounter = 0;
		while (newCounter<1)
		{
			printf("GEN = %d\n", gen);
			printf("Sentence= %s\n", topcandidateSentences[newCounter].sentence);
			printf("Score= %d\n", topcandidateSentences[newCounter].score);
			printf("Rank= %d\n", newCounter+1);
			newCounter++;
		}
		*/
		 
		
	}	
	doSuccessPrints();
}

char randomChar()//returns a random lowercase letter or a space
{
	char newCharacter;
	int r = rand();
	int randomCharacterIndex = r % strlen(alpha);
	
	newCharacter = alpha[randomCharacterIndex];
	return newCharacter;
}

void randomSentence(Candidate *can)
{
	int counter =0;
	
	while(counter<TARGET_LEN-1)
	{
		can->sentence[counter] = randomChar();
		counter++;
	}
	can->sentence[counter] = '\0';
}


//good
void scoreSentences(Candidate *can)
{
	
	int scores=0;
	int counter =0;
	char correct[TARGET_LEN+1]; 

	strcpy(correct, TARGET);

	while (counter < TARGET_LEN) {

		if(can->sentence[counter] == correct[counter])
		{
			scores++;
			
		}
		counter++;
	}
	
	can->score = scores;
}


void breedSentences(Candidate *can1, Candidate *can2, int index)
{
	

	char* childOne = (char*) malloc(sizeof(topcandidateSentences[0].sentence));
	char* childTwo = (char*) malloc(sizeof(topcandidateSentences[0].sentence));
	
	int random = rand(); 
	
	int startPoint  = random %TARGET_LEN-1;
	int childCharacterIndex = startPoint;
	
	while (childCharacterIndex < TARGET_LEN) 
	{
		childOne[childCharacterIndex] = can1->sentence[childCharacterIndex];
		childTwo[childCharacterIndex] = can2->sentence[childCharacterIndex];
		childCharacterIndex++;
	}
		
	childCharacterIndex = startPoint-1;
	
	while (childCharacterIndex >= 0) 
	{
		childTwo[childCharacterIndex] = can1->sentence[childCharacterIndex];
		childOne[childCharacterIndex] = can2->sentence[childCharacterIndex];
		childCharacterIndex--;
	}						

	
	strcpy(topcandidateSentences[index].sentence, childOne);
	strcpy(topcandidateSentences[index-1].sentence, childTwo);
	
}

int checkSentences()
{
	int checkCounter =0;
	while (checkCounter < NUM_CANDIDATES ) 
	{
		if (strcmp(topcandidateSentences[checkCounter].sentence, TARGET) == 0)
		{
			return 1;
		}
		checkCounter++;
	}
	return 0;
}


void doSuccessPrints()
{
	printf("SUCCESS!!!!!!\n");
}








